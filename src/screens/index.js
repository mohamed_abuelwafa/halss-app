import { Navigation } from 'react-native-navigation';

import HomeScreen from './home-screen';
import CategoriesScreen from './categories-screen';
import PostScreen from './post-screen';
import CategoryScreen from './category-screen';

export function registerScreens() {
    Navigation.registerComponent('homeScreen', () => HomeScreen);
    Navigation.registerComponent('categoriesScreen', () => CategoriesScreen);
    Navigation.registerComponent('postScreen', () => PostScreen);
    Navigation.registerComponent('categoryScreen', () => CategoryScreen);
}
